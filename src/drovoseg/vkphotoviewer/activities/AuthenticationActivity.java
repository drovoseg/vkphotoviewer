package drovoseg.vkphotoviewer.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import drovoseg.vkphotoviewer.R;
import drovoseg.vkphotoviewer.internet.vk.VkApi;
import drovoseg.vkphotoviewer.util.Util;


// Активность для кодключения к ВКонтакте
public final class AuthenticationActivity extends Activity {

    // Шаблон URL для выполнения авторизации пользователя в приложении
    private static final String VK_AUTHORIZATION_FORMAT_STRING =
            "https://oauth.vk.com/authorize?"
            + "client_id=%d&"
            + "redirect_uri=https://oauth.vk.com/blank.html&"
            + "display=mobile&"
            + "response_type=token";

    // URL, на который отправляется результат авторизации пользователя в системе
    private static final String REDIRECT_URL = "https://oauth.vk.com/blank.html";

    private WebView webView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.autentification_activity);

        final SharedPreferences preferences = getPreferences(MODE_PRIVATE);

        // Пытаемся получить id сохраненного пользователя
        int userId = preferences.getInt(Util.USER_ID, -1);

        // Если пользоваетль был сохранен, то не выполняя регистрацию,
        // сразу переходим к списку альбомов пользователя
        if (userId != -1)
        {
            VkApi.getInstance().setCurrentUserId(userId);

            startActivity(new Intent(AuthenticationActivity.this, AlbumListActivity.class));
            return;
        }

        webView = (WebView) findViewById(R.id.authentication_activity_webview);

        //Иначе задаем для webView экземпляр класса WebViewClient для отслеживания REDIRECT_URL
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading (WebView view, String url) {

                if (!url.startsWith(REDIRECT_URL)) return false;

                // Был получен адрес, содержащий REDIRECT_URL. Он имеет вид
                // http://REDIRECT_URI#access_token=533bacf01e11f55b536a565b57531ad114461ae8736d6506a3&expires_in=86400&user_id=8492
                // По этой причине отделяем значащую часть полсе # и разбиваем ее на три части по знаку &

                String[] params = url.substring(url.indexOf("#") + 1).split("&");

                // params[2] содержит строку вида user_id=8492. Выделяем из нее числовое значение ID пользователя

                int userId = Integer.parseInt(params[2].substring(params[2].indexOf("=") + 1));

                // Задаем пользователя по-умолчанию
                VkApi.getInstance().setCurrentUserId(userId);

                //Сохраняем информацию о пользоватете в настрйках приложения
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt(Util.USER_ID, userId);
                editor.commit();

                // переходим к списку альбомов полученного пользователя
                startActivity(new Intent(AuthenticationActivity.this, AlbumListActivity.class));

                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (VkApi.getInstance().getCurrentUserId() != -1) return;

        // Создаем URL для авторизации в приложении
        String  authorizationUrl = String.format(VK_AUTHORIZATION_FORMAT_STRING, Util.APP_VK_ID);

        // Приказываем броузеру по нему перейти
        webView.loadUrl(authorizationUrl);
    }
}
