package drovoseg.vkphotoviewer.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import drovoseg.vkphotoviewer.R;
import drovoseg.vkphotoviewer.adapters.PhotosAdapter;
import drovoseg.vkphotoviewer.internet.InternetTask;
import drovoseg.vkphotoviewer.internet.vk.VkApi;
import drovoseg.vkphotoviewer.model.Album;
import drovoseg.vkphotoviewer.model.Photo;
import drovoseg.vkphotoviewer.util.Util;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 30.04.13
 * Time: 21:32
 */

// Активность, предназначенная для просмотра списка фотографий в альбоме
public final class PhotosActivity extends Activity implements AdapterView.OnItemClickListener {

    private static final String LOADED_PHOTOS = "LOADED_PHOTOS";

    private Album album;

    private GridView gridView;
    private ProgressBar progressBar;

    private PhotosAdapter photosAdapter;

    // Создается асинхронная задача, применяемая для получения списка фотографий в альбоме
    private InternetTask<Void, Photo[]> createInternetTask() {
        return new InternetTask<Void, Photo[]>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                progressBar.setVisibility(View.VISIBLE);
                gridView.setVisibility(View.INVISIBLE);
            }

            @Override
            protected Photo[] backgroundInternetRequest(Void... params) {

                //В фоновом потоке получаем список фотографий в указанном альбоме
                // для текущего пользователя
                return VkApi.getInstance().getPhotos(album.getAlbumId());
            }

            @Override
            protected void onPostExecute(Photo[] result) {
                super.onPostExecute(result);

                //Если результат равен null, то во время запроса произошло исключение.
                // В соотвествии с типом исключения формируется сообщение пользователю
                if (result == null)
                {
                    int messageId = 0;
                    RequestResult requestResult = getRequestResult();

                    // Была разоравана связь с сервером
                    if (requestResult == RequestResult.INVALID_CONNECT)
                    {
                        messageId = R.string.connection_error;
                    }

                    //Были получены некорректные данные
                    if (requestResult == RequestResult.PARSING_ERROR)
                    {
                        messageId = R.string.parsing_error;
                    }

                    Toast.makeText(PhotosActivity.this,
                            messageId,
                            Toast.LENGTH_LONG)
                            .show();

                    progressBar.setVisibility(View.INVISIBLE);

                    return;
                }

                // Если был получен резульатт отличный от null

                progressBar.setVisibility(View.INVISIBLE);
                gridView.setVisibility(View.VISIBLE);         //Делаем видимым таблицу фотографий

                // Для таблицы задаем адаптер для форирования ячеек
                photosAdapter = new PhotosAdapter(
                        PhotosActivity.this,
                        result);

                gridView.setAdapter(photosAdapter);
            }
        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        // Получаем альбом, выбранный в списке на предыдущем экране
        album = intent.getParcelableExtra(Util.ALBUM);

        setContentView(R.layout.photos_activity);

        gridView = (GridView) findViewById(R.id.photos_activity_gridview);
        progressBar = (ProgressBar) findViewById(R.id.photos_activity_progressbar);

        gridView.setOnItemClickListener(this);

        ArrayList<Photo> photos = null;

        //Получаем список сохранных фотографий
        if (savedInstanceState != null)
        {
            photos = savedInstanceState.getParcelableArrayList(LOADED_PHOTOS);
            if (photos != null) photosAdapter = new PhotosAdapter(this, photos);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // был ли загружен список фотографий?
        if (photosAdapter == null)
        {
            createInternetTask().execute(new Void[] {});  // Если нет, запускаем задачу для получения списка фотографий
        }
        else
        {
            // Если да, то создаем адаптер на основе списка сохраненных фотографий
            progressBar.setVisibility(View.INVISIBLE);
            gridView.setAdapter(photosAdapter);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //Сохранение массива загруженных фотографий во временное хранилище на диске
        if (photosAdapter != null)
        {
            outState.putParcelableArrayList(LOADED_PHOTOS, photosAdapter.toArrayList());
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        // Получаем текущую фотографию
        Photo photo = photosAdapter.getItem(position);

        // Если миниатюра фотогрфии была загружена, то разрешаем перейти
        // к просотру фотографии на весь экран
        if (photo.getThumbnail() == null) return;

        Intent intent = new Intent(this, PhotoViewActivity.class);
        intent.putExtra(Util.PHOTO, photo);

        startActivity(intent);
    }
}