package drovoseg.vkphotoviewer.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.Toast;
import drovoseg.vkphotoviewer.R;
import drovoseg.vkphotoviewer.internet.InternetTask;
import drovoseg.vkphotoviewer.model.Photo;
import drovoseg.vkphotoviewer.util.Util;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 03.05.13
 * Time: 8:27
 */


// Активность для просмотра фотографии на весь экран+
public final class PhotoViewActivity extends Activity {

    private Photo displayPhoto;

    private ImageView imageView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.photo_view_activity);
        imageView = (ImageView) findViewById(R.id.photo_view_activity_image_view);

        // Получаем фото, выбранное на предыдущем экране
        displayPhoto = getIntent().getParcelableExtra(Util.PHOTO);

        // Выводим на экран превью изображения
        imageView.setImageBitmap(displayPhoto.getThumbnail());
    }

    protected void onResume() {
        super.onResume();

        // Запускаем асинхронну задачу
        createInternetTask().execute(new Void[] {});
    }

    // создаем асинхронную задачу для получения исходного избражения
    private InternetTask<Void, Bitmap> createInternetTask() {
        return new InternetTask<Void, Bitmap>() {

            @Override
            protected Bitmap backgroundInternetRequest(Void... params) {

                // В фоновом потоке загружаем полноразмереную версию фотографии
                displayPhoto.loadPhoto();
                return displayPhoto.getPhoto();
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                super.onPostExecute(result);

                //Если результат равен null, то во время запроса произошло исключение.
                // В соотвествии с типом исключения формируется сообщение пользователю
                if (result == null)
                {
                    int messageId = 0;
                    RequestResult requestResult = getRequestResult();

                    // Была разоравана связь с сервером
                    if (requestResult == RequestResult.INVALID_CONNECT)
                    {
                        // Ничего не делаем, оставляем миниатюру
                        return;
                    }

                    //Были получены некорректные данные
                    if (requestResult == RequestResult.PARSING_ERROR)
                    {
                        messageId = R.string.parsing_error;
                    }

                    // Выдаем сообщение об ошибке
                    Toast.makeText(PhotoViewActivity.this,
                            messageId,
                            Toast.LENGTH_LONG)
                            .show();

                    // Для imageView задаем изображение из ресурсов, свидетельствующее об ошибке
                    imageView.setImageResource(R.drawable.error_icon_white);

                    return;
                }

                // Если ошибок не было, выводим загруженное изображение на весь экран
                imageView.setImageBitmap(result);
            }
        };
    }
}