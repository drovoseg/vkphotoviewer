package drovoseg.vkphotoviewer.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import drovoseg.vkphotoviewer.R;
import drovoseg.vkphotoviewer.adapters.AlbumListAdapter;
import drovoseg.vkphotoviewer.internet.InternetTask;
import drovoseg.vkphotoviewer.internet.vk.VkApi;
import drovoseg.vkphotoviewer.model.Album;
import drovoseg.vkphotoviewer.util.Util;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 28.04.13
 * Time: 1:36
 */


// Активность, предназначенная для просмотра списка фотоальбомов
public final class AlbumListActivity extends Activity implements AdapterView.OnItemClickListener {

    private static final String LOADED_ALBUMS = "LOADED_ALBUMS";

    private AlbumListAdapter listAdapter;

    private ProgressBar progressBar;
    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.album_list_activity);

        progressBar = (ProgressBar) findViewById(R.id.album_list_activity_progressbar);
        listView = (ListView) findViewById(R.id.album_list_activity_listview);

        listView.setOnItemClickListener(this);

        ArrayList<Album> albums = null;

        //Получаем список сохранных альбомов
        if (savedInstanceState != null)
        {
            albums = savedInstanceState.getParcelableArrayList(LOADED_ALBUMS);
            if (albums != null) listAdapter = new AlbumListAdapter(this, albums);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // был ли загружен список альбомов?
        if (listAdapter == null)
        {
            createInternetTask().execute(new Void[] {});   // Если нет, запускаем задачу для получения списка альбомов
        }
        else
        {
            // Если да, то создаем адаптер на основе массива сохраненных альбомов
            progressBar.setVisibility(View.INVISIBLE);
            listView.setAdapter(listAdapter);
        }
    }

    // Создает экземпляр асинхронной задачи, нужный для получения списка альбомов пользователя
    private InternetTask<Void, Album[]> createInternetTask() {
        return new InternetTask<Void, Album[]>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                progressBar.setVisibility(View.VISIBLE);
                listView.setVisibility(View.INVISIBLE);
            }

            @Override
            protected Album[] backgroundInternetRequest(Void... params) {

                //В фоновом потоке получаем список свех альбомов
                // пользователя по-умолчанию, включая системные
                return VkApi.getInstance().getAlbumsList(true);
            }

            @Override
            protected void onPostExecute(Album[] result) {
                super.onPostExecute(result);

                //Если результат равен null, то во время запроса произошло исключение.
                // В соотвествии с типом исключения формируется сообщение пользователю
                if (result == null)
                {
                    int messageId = 0;
                    RequestResult requestResult = getRequestResult();

                    // Была разоравана связь с сервером
                    if (requestResult == RequestResult.INVALID_CONNECT)
                    {
                        messageId = R.string.connection_error;
                    }

                    //Были получены некорректные данные
                    if (requestResult == RequestResult.PARSING_ERROR)
                    {
                        messageId = R.string.parsing_error;
                    }

                    Toast.makeText(AlbumListActivity.this,
                            messageId,
                            Toast.LENGTH_LONG)
                            .show();

                    progressBar.setVisibility(View.INVISIBLE);

                    return;
                }

                // Если был получен резульатт отличный от null

                progressBar.setVisibility(View.INVISIBLE);
                listView.setVisibility(View.VISIBLE);         //Делаем видимым список альбомов

                // Для списка задаем адаптер создания элементов списка
                listAdapter = new AlbumListAdapter(AlbumListActivity.this, result);
                listView.setAdapter(listAdapter);
            }
        };
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //Сохранение массива загруженных альбомов во временное хранилище на диске
        if (listAdapter != null)
        {
            outState.putParcelableArrayList(LOADED_ALBUMS, listAdapter.toArrayList());
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

        // Получаем текущий альбом
        Album album = listAdapter.getItem(position);

        if (album.getThumbnail() == null) return;

        // Если миниатюра альбома была загружена, то разрешаем перейти
        // к списку всех фотографий альбома
        Intent intent = new Intent(this, PhotosActivity.class);

        // Сохранение выбраннго альбома
        intent.putExtra(Util.ALBUM, album);

        startActivity(intent);
    }

    @Override
    public void onBackPressed() {

        //Переходим на главный экран устройства
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
//        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }
}