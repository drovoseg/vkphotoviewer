package drovoseg.vkphotoviewer.util;

import android.content.res.Resources;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 01.05.13
 * Time: 13:45
 * To change this template use File | Settings | File Templates.
 */

// Класс, содержащий все общедоступные константы приложения
public final class Util {

    public static final String APP_LOG_TAG = "VkPhotoViewer";

    public static final String ALBUM = "ALBUM";
    public static final String PHOTO = "PHOTO";
    public static final String USER_ID = "USER_ID";
    public static final int APP_VK_ID = 3623900;

    private Util() {

    }
}
