package drovoseg.vkphotoviewer.util.exceptions;

import org.xml.sax.SAXException;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 06.05.13
 * Time: 4:13
 */

// Класс-обертка над экземпляром SAXException для генерации исключения времени выполнения
public final class RuntimeSAXException extends RuntimeException {
    public RuntimeSAXException() {
        super();
    }

    public RuntimeSAXException(String detailMessage) {
        super(detailMessage);
    }

    public RuntimeSAXException(String detailMessage, SAXException ex) {
        super(detailMessage, ex);
    }

    public RuntimeSAXException(SAXException ex) {
        super(ex);
    }
}
