package drovoseg.vkphotoviewer.util.exceptions;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 02.05.13
 * Time: 20:22
 */

// Класс-обертка над экземпляром IOException для генерации исключения времени выполнения
public final class RuntimeIOException extends RuntimeException {

    public RuntimeIOException() {
        super();
    }

    public RuntimeIOException(String detailMessage) {
        super(detailMessage);
    }

    public RuntimeIOException(String detailMessage, IOException ex) {
        super(detailMessage, ex);
    }

    public RuntimeIOException(IOException ex) {
        super(ex);
    }
}
