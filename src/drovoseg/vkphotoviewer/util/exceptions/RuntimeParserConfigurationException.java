package drovoseg.vkphotoviewer.util.exceptions;

import javax.xml.parsers.ParserConfigurationException;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 06.05.13
 * Time: 3:29
 */

// Класс-обертка над экземпляром ParserConfigurationException для генерации исключения времени выполнения
public final class RuntimeParserConfigurationException extends RuntimeException {
    public RuntimeParserConfigurationException() {
        super();
    }

    public RuntimeParserConfigurationException(String detailMessage) {
        super(detailMessage);
    }

    public RuntimeParserConfigurationException(String detailMessage, ParserConfigurationException ex) {
        super(detailMessage, ex);
    }

    public RuntimeParserConfigurationException(ParserConfigurationException ex) {
        super(ex);
    }
}
