package drovoseg.vkphotoviewer;

import android.app.Application;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 06.05.13
 * Time: 14:37
 */

// Класс, представляющий собой данное приложение.
// Необходим для получения глобальной точки доступа к ресурсам приложения
public final class VkPhotoViewerApplication extends Application {

    private static VkPhotoViewerApplication instance = null;

    public static VkPhotoViewerApplication getInstance() {
        return instance;
    }

    public void onCreate() {
        super.onCreate();

        instance = this;
    }
}
