package drovoseg.vkphotoviewer.internet;

import android.os.AsyncTask;
import drovoseg.vkphotoviewer.util.exceptions.RuntimeIOException;
import drovoseg.vkphotoviewer.util.exceptions.RuntimeSAXException;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 01.05.13
 * Time: 14:09
 */

// Класс, инкапсулирующий в себе обработку ошибок, возникающих при запросе к VK API
public abstract class InternetTask<Params, Result> extends AsyncTask<Params, Void, Result> {

    // Перечисление, описываюющее результат обработки  сетевого запроса
    public static enum RequestResult {

        // Запрос был выполнен
        ALL_OK,

        // Не удалось подключиться к серверу
        INVALID_CONNECT,

        // Ответ от сервера имеет неверный формат
        PARSING_ERROR
    }

    private RequestResult requestResult = RequestResult.ALL_OK;

    public RequestResult getRequestResult() {
        return requestResult;
    }

    @Override
    protected final Result doInBackground(Params... params) {
        try
        {
            // Выполняем асинхронный запрос
            return backgroundInternetRequest(params);
        }
        catch (RuntimeIOException e)
        {
            // Произошел разрыв соединения, или было исчерпано время необходимое для подключения
            requestResult = RequestResult.INVALID_CONNECT;
            return null;
        }
        catch (RuntimeSAXException ex)
        {
            // Ответ от сервера содержит ошибки
            requestResult = RequestResult.PARSING_ERROR;
            return null;
        }
    }

    // Метод, в котором осуществляетсяч асинхронный сетевой запрос
    protected abstract Result backgroundInternetRequest(Params... params);
}
