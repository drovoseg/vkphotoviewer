package drovoseg.vkphotoviewer.internet.vk;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 30.04.13
 * Time: 11:20
 * To change this template use File | Settings | File Templates.
 */

// Исключительная ситуация, выбрасываемая при получении сообщения об ошибке в ответ на запрос к VK API
public final class VkException extends RuntimeException {

    private int errorCode;

    public int getErrorCode() {
        return errorCode;
    }

    public VkException(int errorCode, String message) {
        super(message);

        this.errorCode = errorCode;
    }

    public VkException(int errorCode, String message, Throwable throwable) {
        super(message, throwable);
    }
}
