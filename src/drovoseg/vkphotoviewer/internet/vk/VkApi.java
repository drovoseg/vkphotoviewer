package drovoseg.vkphotoviewer.internet.vk;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;
import android.util.Log;
import drovoseg.vkphotoviewer.R;

import drovoseg.vkphotoviewer.VkPhotoViewerApplication;
import drovoseg.vkphotoviewer.model.Photo;
import drovoseg.vkphotoviewer.util.Util;
import drovoseg.vkphotoviewer.model.Album;
import drovoseg.vkphotoviewer.util.exceptions.RuntimeIOException;
import drovoseg.vkphotoviewer.util.exceptions.RuntimeParserConfigurationException;
//import nu.xom.*;
import drovoseg.vkphotoviewer.util.exceptions.RuntimeSAXException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 28.04.13
 * Time: 10:18
 */

// Класс, который беспечивает доступ к ВКонтакте API для приложения
public final class VkApi {

    // Описывает максимальную задержку по времени (в милисекундах) во время сетевых запросов
    public static int ConnectionTimeouts = 15000;

    // Описывает шаблон для постоения запросов к серверу ВКонтакте
    private String vkApiMethodsFormatString = "https://api.vk.com/method/%s.xml?%s&lang=en";

    // Класс, обеспечивающий выполнение запросов к VK API по протоколу HHTPS
    private HttpClient httpClient = AndroidHttpClient.newInstance(null);

    // ID пользователя по-умолчанию
    private int currentUserId = -1;

    // Построитель ответа от сервера ВКонтакте в формате XML
    DocumentBuilder documentBuilder;

    // Единственный экземпляр класса VkApi
    private static final VkApi instance = new VkApi();

    // Метод возвращает единственный экземпляр класса для использования в приложении
    public static synchronized VkApi getInstance() {
        return instance;
    }

    public int getCurrentUserId() {
        return currentUserId;
    }

    public void setCurrentUserId(int value) {
        if (value <= 0)
        {
            Resources res = VkPhotoViewerApplication.getInstance().getResources();
            throw new IllegalArgumentException(res.getString(R.string.vkapi_getalbumlist_illegal_userid));
        }

        currentUserId = value;
    }

    private VkApi() {

        // Задаем настройки для httpClient
        HttpParams p = httpClient.getParams();

        HttpConnectionParams.setConnectionTimeout(p, ConnectionTimeouts);
        HttpConnectionParams.setSoTimeout(p, ConnectionTimeouts);

        // Послучаем шаблон построения запросов, соответствующий текущим языковым настройкам
        Resources res = VkPhotoViewerApplication.getInstance().getResources();
        vkApiMethodsFormatString = res.getString(R.string.vk_api_methods_format_string);

        try
        {
            documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        }
        catch (ParserConfigurationException e)
        {
            e.printStackTrace();
            Log.e(Util.APP_LOG_TAG, e.getMessage(), e);
            throw new RuntimeParserConfigurationException(e);
        }
    }

    // Метод возвращает поток, содержащий ответ от сервера.
    // Принимает URL-запрос к серверу и MIME-тип возвращаемых данных
    private InputStream getResponseStream(String urlCommand, String mimeType) {

        // Задаем GET-запрос к VK API
        HttpUriRequest request = new HttpGet(urlCommand);

        // задаем MIME-тип возвращаемых данных
        request.addHeader("Accept", mimeType);

        try
        {
            // Выполняем запрос к серверу
            HttpResponse response = httpClient.execute(request);

            // возвращаем результат в виде потока
            return response.getEntity().getContent();
        }
        catch (IOException ex)
        {
            // Во время запроса произошла ошибка соединения
            ex.printStackTrace();
            Log.e(Util.APP_LOG_TAG, ex.getMessage(), ex);
            throw new RuntimeIOException(ex);
        }
    }

    // Метод принимает метод API ВКонтакте в виде URL, и возвращает ответ в виде XML-документа
    private Document getXmlResponse(String urlCommand) {

        Document document = null;

        try
        {
            InputStream inputStream = getResponseStream(urlCommand, "text/xml");

            // Создаем структуру XML-документа на основе потока данных
            document = documentBuilder.parse(inputStream);

            inputStream.close();
        }
        catch (SAXException e)
        {
            // Возникла ошибка соединения
            e.printStackTrace();
            Log.e(Util.APP_LOG_TAG, e.getMessage(), e);
            throw new RuntimeSAXException(e);
        }
        catch (IOException e)
        {
            // Возникла ошибка парсинга ответе от сервра
            e.printStackTrace();
            Log.e(Util.APP_LOG_TAG, e.getMessage(), e);
            throw new RuntimeIOException(e);
        }

        // Проверяем, является ли ответ сервера сообщением об ошибке
        assertXmlResponse(document);

        return document;
    }

    // Метод проверяет, является ли ответ сервера сообщением об ошибке. Если нет, метод не делает ничего.
    // Если ответ содержит ошибку, выбрасывается VkException
    private void assertXmlResponse(Document document) {

        final String ERROR_TAG = "error";
        final String ERROR_CODE_TAG = "error_code";
        final String ERROR_MSG_TAG = "error_msg";

        // Здесь выполняется проверка, является ли корневой тег тегом <error>
        Element root = document.getDocumentElement();
        if (root.getTagName().equals(ERROR_TAG))
        {
            // если да, то формируется VkException на основе содержимого тегов <error_code> и <error_msg>
            int errorCode = Integer.parseInt(root
                    .getElementsByTagName(ERROR_CODE_TAG).item(0)
                    .getTextContent());
            String message = root.getElementsByTagName(ERROR_MSG_TAG).item(0)
                    .getTextContent();

            throw new VkException(errorCode, message);
        }
    }

    public Album[] getAlbumsList(boolean needSystem) {
        return getAlbumsList(getCurrentUserId(), needSystem);
    }

    // метод возвращает список фотоальбомов указанного пользвателя, с возможностью вернуть системные альбомы
    public Album[] getAlbumsList(int userId, boolean needSystem) {

        final String ALBUM_TAG = "album";
        final String TITLE_TAG = "title";
        final String THUMBNAIL_TAG = "thumb_src";
        final String ALBUM_ID_TAG = "aid";

        if (userId <= 0)
        {
            Resources res = VkPhotoViewerApplication.getInstance().getResources();
            throw new IllegalArgumentException(res.getString(R.string.vkapi_getalbumlist_illegal_userid));
        }

        Album[] albums;
        byte digitalNeedSystem = needSystem ? (byte)1 : (byte)0;

        // Формируем список параметров для запрока к серверу
        String parameters = String.format("oid=%d&need_system=%d&need_covers=1", userId, digitalNeedSystem);

        // Формируем запрос к серверу на получение массива альбомов
        String command = String.format(vkApiMethodsFormatString, "photos.getAlbums", parameters);

        // получаем ответ от сервера в XML формате
        Document doc = getXmlResponse(command);

        // Получаем список узлов XML-документа, соответствующих альбомам
        NodeList albumsNodes = doc.getDocumentElement().getElementsByTagName(ALBUM_TAG);

        albums = new Album[albumsNodes.getLength()];

        // Для каждого узла в списке выполняем чтение ID альбома, его названия и URL иниатюры альбома
        // После чего создаем новый альбом
        for (int i = 0; i < albumsNodes.getLength(); i++)
        {
            Element albumNode = (Element) albumsNodes.item(i);
            int albumId = Integer.parseInt(albumNode
                    .getElementsByTagName(ALBUM_ID_TAG).item(0)
                    .getTextContent());

            String thumbnailUrl = albumNode
                    .getElementsByTagName(THUMBNAIL_TAG).item(0)
                    .getTextContent();

            String title = albumNode
                    .getElementsByTagName(TITLE_TAG).item(0)
                    .getTextContent();

            albums[i] = new Album(albumId, thumbnailUrl, title);
        }

        return  albums;
    }

    // Возвращает список альбомов на основании ID пользователя либо его псевдонима,
    // с возможностью вернуть системные альбомы
    public Album[] getAlbumsList(String userIdOrName, boolean needSystem) {

        int userId = 0;

        try
        {
            // пытаемся привести userIdOrName к числу, предполагая, что это ID пользователя
            userId = Integer.parseInt(userIdOrName);
        }
        catch (NumberFormatException ex)
        {
            // в случае неудачи, выполняем запрос к серверу ВКонтакте
            // чтобы получить ID пользователя по его псевдониму
            final String USER_TAG = "user";
            final String UID_TAG = "uid";

            String command = String.format(vkApiMethodsFormatString, "users.get", "uids=" + userIdOrName);

            Document doc = getXmlResponse(command);
            Element root = doc.getDocumentElement();

            Element node = (Element) root.getElementsByTagName(USER_TAG).item(0);
            userId = Integer.parseInt(node.getElementsByTagName(UID_TAG).item(0).getTextContent());
        }

        // возвращаем массив альбомов
        return  getAlbumsList(userId, needSystem);
    }

    public Photo[] getPhotos(int albumId) {
        return getPhotos(getCurrentUserId(), albumId);
    }

    // Метод позволяет получить массив фотографий пользователя из указанного фотоальбома
    public Photo[] getPhotos(int userId, int albumId) {
        if (userId <= 0)
        {
            Resources res = VkPhotoViewerApplication.getInstance().getResources();
            throw new IllegalArgumentException(res.getString(R.string.vkapi_getalbumlist_illegal_userid));
        }

        final String PHOTO_TAG = "photo";
        final String SRC_TAG = "src";
        final String SRC_BIG_TAG = "src_big";
        final String SRC_SMALL_TAG = "src_small";
        final String SRC_XBIG_TAG = "src_xbig";
        final String SRC_XXBIG_TAG = "src_xxbig";
        final String SRC_XXXBIG_TAG = "src_xxxbig";

        Photo[] albumPhotos;

        // Формируем список параметров для запрока к серверу
        String parameters = "oid=" + userId + "&aid=" + albumId;

        // Формируем запрос к серверу на получение массива фотографий
        String command = String.format(vkApiMethodsFormatString, "photos.get", parameters);

        // получаем ответ от сервера в XML формате
        Document doc = getXmlResponse(command);

        // Получаем список узлов XML-документа, соответствующих отдельным фотографиям
        NodeList photoNodes = doc.getDocumentElement().getElementsByTagName(PHOTO_TAG);

        albumPhotos = new Photo[photoNodes.getLength()];

        // Для каждого узла в списке выполняем чтение URL миниатюры фотографии и URL cамой фотографии
        // После чего создаем новую фотграфию
        for (int i = 0; i < photoNodes.getLength(); i++)
        {
            Element photoNode = (Element) photoNodes.item(i);

            // Получаем URL, по которому расположена миниатюра фотографии
            String thumbnailUrl = photoNode.getElementsByTagName(SRC_TAG).item(0).getTextContent();

            // В этом фрагменте кода мы пытаемся получить наибольший вариант фотографии
            Element photoUrlNode = (Element) photoNode.getElementsByTagName(SRC_XXXBIG_TAG).item(0);

            // Если фотографии с максимальным размером не обнаружено,
            // выполняются попытки чтения фотографий по убыванию размера
            if (photoUrlNode == null) photoUrlNode = (Element) photoNode.getElementsByTagName(SRC_XXBIG_TAG).item(0);
            if (photoUrlNode == null) photoUrlNode = (Element) photoNode.getElementsByTagName(SRC_XBIG_TAG).item(0);
            if (photoUrlNode == null) photoUrlNode = (Element) photoNode.getElementsByTagName(SRC_BIG_TAG).item(0);

            // создается новая фотграфия на основе URL миниатюры и URL самой фотографии
            albumPhotos[i] = new Photo(thumbnailUrl, photoUrlNode.getTextContent());
        }

        return albumPhotos;
    }

    // метод получает URL, по которому расположена фотография, и возвращает саму фотографию в качестве результата
    public Bitmap getPhotoByUrl(String urlString) {

        InputStream inputStream = getResponseStream(urlString, "image/jpeg");
        Bitmap result = BitmapFactory.decodeStream(inputStream);

        try
        {
            inputStream.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw new RuntimeIOException(e);
        }

        return result;
    }
}
