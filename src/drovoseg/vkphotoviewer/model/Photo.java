package drovoseg.vkphotoviewer.model;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import drovoseg.vkphotoviewer.R;
import drovoseg.vkphotoviewer.VkPhotoViewerApplication;
import drovoseg.vkphotoviewer.internet.vk.VkApi;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 01.05.13
 * Time: 14:42
 */

// Сущность, представляющая фотографию
public final class Photo implements Parcelable {


    private String thumbnailUrl;           // URL для загрузки миниатюры фотграфии
    private Bitmap thumbnail;              // сама миниатюра
    private String photoUrl;               // URL для загрузки фотграфии
    private Bitmap photo;                  // сама фотография

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    private void setThumbnailUrl(String thumbnailUrl) {
        if (thumbnailUrl == null)
        {
            Resources res = VkPhotoViewerApplication.getInstance().getResources();
            throw new IllegalArgumentException(res.getString(R.string.set_album_title_exception_message));
        }

        this.thumbnailUrl = thumbnailUrl;
    }

    public  Bitmap getThumbnail() {
        return thumbnail;
    }

    // Метод, выполняющий загрузку миниатюры фотографии по сети
    public void loadThumbnail() {

        // Если миниатюра уже загружена, выходим из метода
        if (thumbnail != null) return;

        // Иначе выполняем загрузку
        thumbnail = VkApi.getInstance().getPhotoByUrl(getThumbnailUrl());
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    private void setPhotoUrl(String photoUrl) {
        if (photoUrl == null)
        {
            Resources res = VkPhotoViewerApplication.getInstance().getResources();
            throw new IllegalArgumentException(res.getString(R.string.set_photo_url_exception_message));
        }
        this.photoUrl = photoUrl;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    // Метод, выполняющий загрузку фотографии по сети
    public void loadPhoto() {

        // Если фотография уже загружена, выходим из метода
        if (photo != null) return;

        // Иначе выполняем загрузку
        photo = VkApi.getInstance().getPhotoByUrl(getPhotoUrl());
    }

    public Photo(String thumbnailUrl, String photoUrl) {
        setThumbnailUrl(thumbnailUrl);
        setPhotoUrl(photoUrl);
    }

    //-----------------методы для обеспечения передачи фото между активностями---------------------

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(getThumbnailUrl());
        parcel.writeString(getPhotoUrl());
        parcel.writeParcelable(getThumbnail(), flags);
        parcel.writeParcelable(getPhoto(), flags);
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel parcel) {

            Photo resPhoto;

            String thumbnailUrl = parcel.readString();
            String photoUrl = parcel.readString();
            Bitmap thumbnail = parcel.readParcelable(null);
            Bitmap photo = parcel.readParcelable(null);

            resPhoto = new Photo(thumbnailUrl, photoUrl);
            resPhoto.thumbnail = thumbnail;
            resPhoto.photo = photo;

            return resPhoto;
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };
}
