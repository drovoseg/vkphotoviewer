package drovoseg.vkphotoviewer.model;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import drovoseg.vkphotoviewer.R;
import drovoseg.vkphotoviewer.VkPhotoViewerApplication;
import drovoseg.vkphotoviewer.internet.vk.VkApi;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 30.04.13
 * Time: 17:33
 */

// Сущность, представляющая фотоальбом
public final class Album implements Parcelable {

    private String title;                 // Название альбома
    private int albumId;                  // его ID
    private String thumbnailUrl;          // URL для загрузки миниатюры
    private Bitmap thumbnail;             // Сама миниатюра

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    private void setThumbnailUrl(String thumbnailUrl) {
        if (thumbnailUrl == null)
        {
            Resources res = VkPhotoViewerApplication.getInstance().getResources();
            throw new IllegalArgumentException(res.getString(R.string.set_thumbnail_url_exception_message));
        }

        this.thumbnailUrl = thumbnailUrl;
    }

    public String getTitle() {
        return title;
    }

    private void setTitle(String title) {
        if (title == null)
        {
            Resources res = VkPhotoViewerApplication.getInstance().getResources();
            throw new IllegalArgumentException(res.getString(R.string.set_album_title_exception_message));
        }

        this.title = title;
    }

    public int getAlbumId() {
        return albumId;
    }

    private void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public  Bitmap getThumbnail() {
        return thumbnail;
    }

    // Метод, выполняющий загрузку миниатюры альбома по сети
    public void loadThumbnail() {

        // Если миниатюра уже загружена, выходим из метода
        if (thumbnail != null) return;

        // Иначе выполняем загрузку
        thumbnail = VkApi.getInstance().getPhotoByUrl(getThumbnailUrl());
    }


    public Album(int albumId, String thumbnailUrl, String title) {
        setAlbumId(albumId);
        setThumbnailUrl(thumbnailUrl);
        setTitle(title);
    }

    //-----------------методы для обеспечения передачи альбома между активностями---------------------

    @Override
    public int describeContents() {
        return 0;
    }


    // Запись альбома во временной хранилище
    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(getTitle());
        parcel.writeInt(getAlbumId());
        parcel.writeString(getThumbnailUrl());
        parcel.writeParcelable(getThumbnail(), flags);
    }

    public static final Creator<Album> CREATOR = new Creator<Album>() {

        // Чтение альбома из временного хранилища
        @Override
        public Album createFromParcel(Parcel parcel) {

            String title = parcel.readString();
            int albumId = parcel.readInt();
            String thumbnailUrl = parcel.readString();
            Bitmap thumbnail = parcel.readParcelable(null);

            Album album = new Album(albumId, thumbnailUrl, title);
            album.thumbnail = thumbnail;

            return album;
        }

        @Override
        public Album[] newArray(int size) {
            return new Album[size];
        }
    };
}
