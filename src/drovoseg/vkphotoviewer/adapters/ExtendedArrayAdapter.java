package drovoseg.vkphotoviewer.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.widget.ArrayAdapter;
import drovoseg.vkphotoviewer.R;
import drovoseg.vkphotoviewer.VkPhotoViewerApplication;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 04.05.13
 * Time: 2:04
 */

// Класс расширееного адаптера, позволяющего получения списка объектов, содержащихся в нем
public class ExtendedArrayAdapter<T> extends ArrayAdapter<T> {
    public ExtendedArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public ExtendedArrayAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public ExtendedArrayAdapter(Context context, int textViewResourceId, T[] objects) {
        super(context, textViewResourceId, objects);

        assertObjects(objects);
    }

    public ExtendedArrayAdapter(Context context, int resource, int textViewResourceId, T[] objects) {
        super(context, resource, textViewResourceId, objects);

        assertObjects(objects);
    }

    public ExtendedArrayAdapter(Context context, int textViewResourceId, List<T> objects) {
        super(context, textViewResourceId, objects);

        assertObjects((T[])objects.toArray());
    }

    public ExtendedArrayAdapter(Context context, int resource, int textViewResourceId, List<T> objects) {
        super(context, resource, textViewResourceId, objects);

        assertObjects((T[])objects.toArray());
    }

    private void assertObjects(T[] objects) {
        if (objects == null)
        {
            Resources res = VkPhotoViewerApplication.getInstance().getResources();
            throw new NullPointerException(res.getString(R.string.null_array_exception_message));
        }
    }

    // Метод преобразования адаптреа к списку
    public ArrayList<T> toArrayList() {

        ArrayList<T> arrayList = new ArrayList<T>(getCount());

        for (int i = 0; i < getCount(); i++)
        {
            arrayList.add(getItem(i));
        }

        return arrayList;
    }
}
