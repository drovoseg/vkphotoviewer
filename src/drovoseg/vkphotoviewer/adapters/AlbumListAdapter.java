package drovoseg.vkphotoviewer.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import drovoseg.vkphotoviewer.R;
import drovoseg.vkphotoviewer.VkPhotoViewerApplication;
import drovoseg.vkphotoviewer.internet.InternetTask;
import drovoseg.vkphotoviewer.model.Album;
import drovoseg.vkphotoviewer.util.Util;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 30.04.13
 * Time: 17:28
 */

// Адаптер для отображения списка фотоальбомов
public final class AlbumListAdapter extends ExtendedArrayAdapter<Album> {

    // Служебный класс для сохранения загруженных элементов списка альбомов
    private static class ViewHolder {
        public ImageView imageView;
        public ProgressBar progressBar;
        public TextView textView;
    }

    private static final int LAYOUT_ID = R.layout.album_list_item;

    public AlbumListAdapter(Context context) {
        super(context, LAYOUT_ID);
    }

    public AlbumListAdapter(Context context, Album[] objects) {
        super(context, LAYOUT_ID, objects);
    }

    public AlbumListAdapter(Context context, List<Album> objects) {
        super(context, LAYOUT_ID, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View resultView = null;
        ViewHolder holder = null;

        if (convertView == null)
        {
            // Если еслемент списка еще не был создан, создаем его на базе ресурса
            resultView = LayoutInflater.from(getContext()).inflate(LAYOUT_ID, null);

            // Создаем холдер для сохранения ссылок на компоненты элемента списка
            holder = new ViewHolder();
            holder.imageView = (ImageView) resultView.findViewById(R.id.album_list_item_album_thumbnail);
            holder.textView = (TextView) resultView.findViewById(R.id.album_list_item_album_title);
            holder.progressBar = (ProgressBar) resultView.findViewById(R.id.album_list_item_thumbnail_progressbar);

            // привязываем служебный объект к элементу списка
            resultView.setTag(holder);
        }
        else
        {
            // Получаем елемент списка
            resultView = convertView;

            // Получаем служебный объект для доступа к элементам GUI
            holder = (ViewHolder)resultView.getTag();
        }

        // Получаем текущий фотоальбом в адаптере
        final Album album = getItem(position);
        final ViewHolder fHolder = holder;

        // Данная задача выполняет загрузку минатюры для альбома по сети
        InternetTask<Void, Bitmap> task = new InternetTask<Void, Bitmap>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                fHolder.imageView.setVisibility(View.INVISIBLE);
                fHolder.progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected Bitmap backgroundInternetRequest(Void... params) {

                // Загружем асинхронно миниатюру для альбома
                album.loadThumbnail();
                return album.getThumbnail();
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                super.onPostExecute(result);

                if (result == null)
                {
                    // Если произошла ошибка при загрузке, задаем в качестве миниатюры изображение из ресурсов
                    fHolder.imageView.setImageResource(R.drawable.error_icon_white);
                }

                // Иначе отображаем загруженную миниатюру
                else fHolder.imageView.setImageBitmap(result);

                fHolder.imageView.setVisibility(View.VISIBLE);
                fHolder.progressBar.setVisibility(View.INVISIBLE);
            }
        };

        // Название альбома не требует асинхронной загрузки, поэтому выводим его сразу
        holder.textView.setText(album.getTitle());

        // Запускаем задчу на выполнение
        task.execute(new Void[] {});

        return resultView;

    }
}