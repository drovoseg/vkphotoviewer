package drovoseg.vkphotoviewer.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import drovoseg.vkphotoviewer.R;
import drovoseg.vkphotoviewer.internet.InternetTask;
import drovoseg.vkphotoviewer.model.Photo;
import drovoseg.vkphotoviewer.util.Util;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 01.05.13
 * Time: 16:30
 */

// Адаптер для отображения списка фотографий
public final class PhotosAdapter extends ExtendedArrayAdapter<Photo> {

    // Служебный класс для сохранения загруженных элементов списка фотографий
    private static class ViewHolder {
        public ImageView imageView;
        public ProgressBar progressBar;
    }

    private static final int LAYOUT_ID = R.layout.photos_grid_item;

    public PhotosAdapter(Context context) {
        super(context, LAYOUT_ID);
    }

    public PhotosAdapter(Context context, Photo[] objects) {
        super(context, LAYOUT_ID, objects);
    }

    public PhotosAdapter(Context context, List<Photo> objects) {
        super(context, LAYOUT_ID, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View resultView = null;
        ViewHolder holder = null;

        if (convertView == null)
        {
            // Если еслемент списка еще не был создан, создаем его на базе ресурса
            resultView = LayoutInflater.from(getContext()).inflate(LAYOUT_ID, null);

            // Создаем холдер для сохранения ссылок на компоненты элемента списка
            holder = new ViewHolder();
            holder.imageView = (ImageView) resultView.findViewById(R.id.photos_grid_item_photo_thumbnail);
            holder.progressBar = (ProgressBar) resultView.findViewById(R.id.photos_grid_item_thumbnail_progressbar);

            // привязываем служебный объект к элементу списка
            resultView.setTag(holder);
        }
        else
        {
            // Получаем елемент списка
            resultView = convertView;

            // Получаем служебный объект для доступа к элементам GUI
            holder = (ViewHolder)resultView.getTag();
        }

        // Получаем текущую фотографию в адаптере
        final Photo photo = getItem(position);
        final ViewHolder fHolder = holder;

        // Данная задача выполняет загрузку минатюры фотографии по сети
        InternetTask<Void, Bitmap> task = new InternetTask<Void, Bitmap>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                fHolder.imageView.setVisibility(View.INVISIBLE);
                fHolder.progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected Bitmap backgroundInternetRequest(Void... params) {

                // Загружем асинхронно миниатюру фотографии
                photo.loadThumbnail();
                return photo.getThumbnail();
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                super.onPostExecute(result);

                if (result == null)
                {
                    // Если произошла ошибка при загрузке, задаем в качестве миниатюры изображение из ресурсов
                    fHolder.imageView.setImageResource(R.drawable.error_icon_white);
                }
                // Иначе отображаем загруженную миниатюру
                else fHolder.imageView.setImageBitmap(result);

                fHolder.imageView.setVisibility(View.VISIBLE);
                fHolder.progressBar.setVisibility(View.INVISIBLE);
            }
        };

        // Запускаем задчу на выполнение
        task.execute(new Void[] {});

        return resultView;
    }
}
